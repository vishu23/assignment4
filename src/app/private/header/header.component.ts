import { Component, OnInit } from '@angular/core'; 
import { Router } from '@angular/router'; 
import { TeamService } from '../../team.service'; 

@Component({ 
selector: 'app-header', 
templateUrl: './header.component.html', 
styleUrls: ['./header.component.css'] 
}) 
export class HeaderComponent implements OnInit { 

constructor(private router: Router, private teamService: TeamService) { } 

ngOnInit() { 
} 

// logout function 
logout() { 
this.teamService.logout().subscribe(response => { 
localStorage.removeItem('accessToken'); 
this.router.navigate(['']); 
}); 
} 

}