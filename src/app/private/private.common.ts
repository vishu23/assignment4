import { Routes } from '@angular/router';
import { TeamComponent } from './team/team.component';
import { EditTeamComponent } from './edit-team/edit-team.component';
import { AddTeamComponent } from './add-team/add-team.component';

export const componentDeclarations: any[] = [
];

export const providerDeclarations: any[] = [
];

export const routes: Routes = [
    {
        path : 'team',
        component : TeamComponent
    },
    {
        path : 'editTeam',
        component : EditTeamComponent
    },
    {
        path : 'addTeam',
        component : AddTeamComponent
    }
];
