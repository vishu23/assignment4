import { Component, OnInit } from '@angular/core';
import { TeamService } from "../../team.service";
import { TotalService } from '~/app/services/total.service';
@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  constructor(public teamservice: TeamService, private sum:TotalService) { }

  teams:any=[];
  total:number;

  ngOnInit() {
    this.teamservice.getTeams().subscribe(res => {
      // console.log(res);
      this.teams=res.data;
    //  console.log(this.teams)
    this.total = res.data.reduce((sum, item) => sum + item.amount, 0); 
   this.sum.count=this.total;
   console.log(this.sum.count)
  

    })
  }
  
}
